/*
 ©2020 Ozvid Technologies Pvt. Ltd. All Rights Reserved.Hosted by jiWebHosting.com
*/

$(window).scroll(function() {
  const scroll = $(window).scrollTop();
  if (scroll >= 100) {
    $("body").addClass("stickys");
  } else {
    $("body").removeClass("stickys");
  }
});

$(".tg-categoriesslider").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 4
    }
  }
});
$(".tg-ads-new").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 2
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
});
$(document).ready(function() {
  $(".search-bar").click(function() {
    $(".search-area").toggle();
    const lastScrollTop = 0;
    $(window).scroll(function(event) {
      const st = $(this).scrollTop();
      if (st > lastScrollTop) {
        $(".search-area").css("display", "none");
      } else {
        // upscroll code
      }
      lastScrollTop = st;
    });
    $("#hide-btn").click(function() {
      $(".search-area").css("display", "none");
    });
  });
});

$(document).ready(function() {
  $(".country-ph").click(function() {
    $(".country-div").toggle();
    $(".country-li").click(function() {
      $(".country-div").css("display", "none");
    });
  });
});
