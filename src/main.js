
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Toasted from "vue-toasted";
import "./registerServiceWorker";
import { CHECK_AUTH } from "./store/actions.type";
import ApiService from "./common/api.service";
import DateFilter from "./common/date.filter";
import ErrorFilter from "./common/error.filter";
import Vuelidate from "vuelidate";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import Paginate from "vuejs-paginate";
import CxltToastr from "cxlt-vue2-toastr";
import "cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css";
import VueToast from "vue-toast-notification";
import moment from "moment";
//import BlockUI from "vue-blockui";

Vue.component("paginate", Paginate);
Vue.use(Vuelidate);
Vue.use(VueSweetalert2);
Vue.use(VueToast);
Vue.use(CxltToastr);
//Vue.use(BlockUI);
Vue.use(Toasted), (Vue.config.productionTip = false);
Vue.filter("date", DateFilter);
Vue.filter("error", ErrorFilter);
Vue.filter("formatDate", function(value) {
  if (value) {
    return moment(String(value)).format("MMMM DD YYYY hh:mm");
  }
});
ApiService.init();

// Ensure we checked auth before each page load.

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
