
import {
  getTokenFromLocalStorage,
  saveTokenToLocalStorage,
  saveUserToLocalStorage,
  getUserFromLocalStorage,
  destroyTokenFromLocalStorage
} from "./helper";
import { ID_TOKEN_KEY, data } from "./config";

export const getToken = () => {
  return getTokenFromLocalStorage(ID_TOKEN_KEY);
  // return getTokenFromLocalStorage(data);
};

export const saveToken = token => {
  saveTokenToLocalStorage(ID_TOKEN_KEY, token);
};

export const saveUser = user => {
  saveUserToLocalStorage(data, user);
};

export const getUser = () => {
  return getUserFromLocalStorage(data);
};

export const destroyToken = () => {
  destroyTokenFromLocalStorage(ID_TOKEN_KEY);
};

export default { getToken, saveToken, destroyToken, saveUser, getUser };
