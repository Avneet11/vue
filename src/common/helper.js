import { data } from "./config";


export const getTokenFromLocalStorage = ID_TOKEN_KEY => {
  return window.localStorage.getItem(ID_TOKEN_KEY);
};
export const getUserFromLocalStorage = data => {
  return window.localStorage.getItem(data);
};

export const saveTokenToLocalStorage = (ID_TOKEN_KEY, token) => {
  window.localStorage.setItem(ID_TOKEN_KEY, token);
};

export const saveUserToLocalStorage = (data, user) => {
  window.localStorage.setItem(data, user);
};

export const destroyTokenFromLocalStorage = () => {
  window.localStorage.clear();
};

export default {
  getTokenFromLocalStorage,
  saveTokenToLocalStorage,
  saveUserToLocalStorage,
  getUserFromLocalStorage,
  destroyTokenFromLocalStorage
};
