
import ApiService from "@/common/api.service";
import JwtService from "@/common/jwt.service";

import {
  LOGIN,
  LOGOUT,
  REGISTER,
  CHECK_AUTH,
  SEARCH,
  UPDATE_USER,
  CONTACT,
  ADDITEM,
  EDITITEM,
  UPDATEPROFILE,
  PRODUCTS,
  CATEGORIES,
  ITEMDETAIL,
  CHANGEPASSWORD,
  MYADS,
  FORGOTPASSWORD,
  FORGOTPASSWORDRESET,
  DELETEAD
} from "./actions.type";

import { SET_AUTH, PURGE_AUTH, SET_ERROR } from "./mutations.type";

const state = {
  errors: null,
  success: null,
  user: JwtService.getUser(),
  token: JwtService.getToken(),
  isAuthenticated: JwtService.getToken()
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  },
  token(state) {
    return state.token;
  }
};

const actions = {
  [LOGIN](context, credentials) {
    var { email, password } = credentials;
    return new Promise((resolve, reject) => {
      ApiService.post("/login", { email, password }).then(({ data }) => {
        if (!data.message === "Success") {
          context.commit(SET_ERROR, data.message);
          return true;
        }
        context.commit(SET_AUTH, data);
        resolve(data);
      });
    });
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  [CONTACT](context, user) {
    return new Promise((resolve, reject) => {
      ApiService.setHeader();
      ApiService.post("/contactus", {
        first_name: user.first_name,
        last_name: user.first_name,
        email_id: user.email_id,
        subject: user.subject,
        comment: user.comment
      })
        .then(({ data }) => {
          context.commit(SET_AUTH, data.data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [REGISTER](context, user) {
    return new Promise((resolve, reject) => {
      ApiService.post("/register", {
        first_name: user.first_name,
        middle_name: user.middle_name,
        last_name: user.last_name,
        email: user.email,
        password: user.password,
        phone: user.phone,
        socialLogin: {}
      })
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            resolve(data);
            return true;
          }
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [SEARCH](context, user) {
    return new Promise((resolve, reject) => {
      ApiService.post("/search", { user })
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [ITEMDETAIL](context, params) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.get("/item/" + params).then(({ data }) => {
        if (!data.success) {
          context.commit(SET_ERROR, data.success);
          return true;
        }
        //context.commit(data);
        resolve(data);
      });
    });
  },
  [PRODUCTS](context, user) {
    return new Promise((resolve, reject) => {
      ApiService.get("/allads" + user)
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [DELETEAD](context, id) {
    return new Promise((resolve, reject) => {
      ApiService.delete("/delete/" + id)
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [MYADS](context, user) {
    return new Promise((resolve, reject) => {
      ApiService.setHeader();
      ApiService.get("/singleUserAds")
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [CATEGORIES](context, user) {
    return new Promise((resolve, reject) => {
      ApiService.get("/catgorieslist")
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },

  [FORGOTPASSWORD](context, user) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.post("/forgotPassword", user).then(({ data }) => {
        if (!data.success) {
          context.commit(SET_ERROR, data.message);
          return true;
        }
        context.commit(data);
        resolve(data);
      });
    });
  },
  [FORGOTPASSWORDRESET](context, user) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.post("/forgotPasswordReset", user).then(({ data }) => {
        if (!data.success) {
          context.commit(SET_ERROR, data.message);
          return true;
        }
        context.commit(data);
        resolve(data);
      });
    });
  },
  [ADDITEM](context, user) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.post("/addItem", user)
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          context.commit(data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [CHECK_AUTH](context) {
    if (JwtService.getToken()) {
      ApiService.setHeader();
      ApiService.get("user")
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [UPDATEPROFILE](context, user) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.put("/userUpdate", user)
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          context.commit(data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [EDITITEM](context, user) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.put("/updateItem", user)
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          context.commit(data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },
  [CHANGEPASSWORD](context, user) {
    return new Promise((resolve, reject) => {
      //set header
      ApiService.setHeader();
      ApiService.post("/changePassword", user)
        .then(({ data }) => {
          if (!data.success) {
            context.commit(SET_ERROR, data.message);
            return true;
          }
          context.commit(data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.statusText);
          reject(response);
        });
    });
  },

  [UPDATE_USER](context, payload) {
    const { email, username, password, image, bio } = payload;
    const user = {
      email,
      username,
      bio,
      image
    };
    if (password) {
      user.password = password;
    }

    return ApiService.put("user", user).then(({ data }) => {
      context.commit(SET_AUTH, data.user);
      return data;
    });
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
    state.success = null;
  },

  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = null;
    state.success = "";
    state.token = user.token;
    JwtService.saveToken(user.token);
    JwtService.saveUser(JSON.stringify(user.data));
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    state.token = false;
    JwtService.destroyToken();
    state.success = "Logout Success";
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
