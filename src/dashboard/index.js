
import store from "../store/auth.module";

const ifAuthenticated = (to, from, next) => {
  if (store.state.token) {
    next();
    return;
  }
  next("/login");
};

export default {
  name: "dashboard",
  path: "/dashboard",
  beforeEnter: ifAuthenticated,
  component: () => import("@/views/Dashboard"),
  redirect: "/dashboard/dashboardHome",
  children: [
    {
      name: "dashboardHome",
      path: "dashboardHome",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardHome")
    },
    {
      name: "dashboardProfileSetting",
      path: "dashboardProfileSetting",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardProfileSetting")
    },
    {
      name: "dashboardMyAds",
      path: "dashboardMyAds",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardMyAds")
    },
    {
      name: "dashboardPostAd",
      path: "dashboardPostAd",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardPostAd")
    },
    {
      name: "dashboardPayments",
      path: "dashboardPayments",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardPayments")
    },
    {
      name: "dashboardMyFavorite",
      path: "dashboardMyFavorite",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardMyFavorite")
    },
    {
      name: "dashboardAccountSetting",
      path: "dashboardAccountSetting",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/DashboardAccountSetting")
    }
  ]
};
