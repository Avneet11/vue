
import React from "react";
import { div, Row, Col, Image, Form, Table, Card } from "react-bootstrap";
import Navbarmain from "../navbar";
import Footer from "../footer";

export default class HomeBody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <Navbarmain />
        <div className="header-joint upload-hd-image">
          <Image
            className="w-100 banner-upload"
            src="assets/images/upload-track.png"
          />
        </div>
        <div className="allbody-main">
          <div className="second-section">
            <div className="container-fluid">
              <Row>
                <Col xl={3} className="set-width">
                  <div className="bg-white user-profile left-outer upload-hd-top px-4 pt-3">
                    <Row>
                      <Col sm={4}>
                        <div className="left-text">
                          <p>
                            <a className="blue-color" href="#0">
                              Listened{" "}
                            </a>
                          </p>
                          <p className="mt-2">
                            <a href="#0">555k </a>
                          </p>
                        </div>
                      </Col>
                      <Col sm={4}>
                        <div className="d-flex justify-content-center">
                          <Image
                            className="w-20"
                            src="assets/images/Statistic-Logo.png"
                          />
                        </div>
                      </Col>
                      <Col sm={4}>
                        <div className="left-text">
                          <p>
                            <a className="blue-color" href="#0">
                              Downloaded{" "}
                            </a>
                          </p>
                          <p className="mt-2">
                            <a href="#0">178k</a>
                          </p>
                        </div>
                      </Col>
                    </Row>
                    <Row className="py-3">
                      <Col sm={12}>
                        <div className="profile-text">
                          <div className="p--pic">
                            <Image
                              className="profile-pics"
                              src="assets/images/profile/PlayrA2.png"
                            />
                            <div className=" field-choose-file">
                              <input
                                name="User[profile_file]"
                                value=""
                                type="hidden"
                              />
                              <input
                                id="choose-file"
                                className="choose-file"
                                name="User[profile_file]"
                                type="file"
                              />
                              <label
                                htmlFor="choose-file"
                                className="upload-file"
                              >
                                <Image
                                  className="file-icons"
                                  src="assets/images/profile/Photo.png"
                                />
                              </label>
                            </div>
                          </div>
                          <h5 className="mb-0">Artist</h5>
                        </div>
                      </Col>
                    </Row>
                    <Row className="py-3">
                      <Col sm={12}>
                        <div className="artist-section">
                          <ul className="mb-0">
                            <li>
                              <Image src="assets/images/profile/Friends.png" />
                              <span className="ml-3">
                                <a href="#0">Friends</a>
                              </span>
                            </li>
                            <li>
                              <Image src="assets/images/profile/2.png" />
                              <span className="ml-3">
                                <a href="#0">Message</a>
                              </span>
                            </li>
                            <li>
                              <Image src="assets/images/profile/my-tracks.png" />
                              <span className="ml-3">
                                <a href="#0">tracks</a>
                              </span>
                            </li>
                            <li>
                              <Image src="assets/images/profile/Card.png" />
                              <span className="ml-3">
                                <a href="#0">money</a>
                              </span>
                            </li>
                          </ul>
                        </div>
                      </Col>
                    </Row>
                    <div className="py-2">
                      <a
                        href="#0"
                        className="custom-btn btn btn-primary btn-block f-med"
                      >
                        <Image
                          className="w-10px mr-3"
                          src="assets/images/profile/arrow.png"
                        />
                        Upload track
                      </a>
                    </div>
                    <div className="mt-auto pt-2">
                      <ul className="social-link">
                        <li>
                          <a href="#">
                            <Image src="assets/images/profile/icos.png" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <Image src="assets/images/profile/facebook.png" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <Image src="assets/images/profile/soundcloud.png" />
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="any-que d-flex">
                      <a href="#">
                        <Image
                          className="w-25px"
                          src="assets/images/ST Rules.png"
                        />
                      </a>
                      <a className="ml-auto" href="#">
                        <Image
                          className="w-25px"
                          src="assets/images/settings.png"
                        />
                      </a>
                    </div>
                  </div>
                </Col>
                <Col xl={6}>
                  <div className="my-4 bg-white">
                    <div className="artist-track mb-3">
                      <Row>
                        <Col sm={2} className="pr-md-0">
                          <div className="position-relative uploadtrack-image">
                            <Image src="assets/images/upload-play.png" />
                          </div>
                        </Col>
                        <Col sm={10} className="pl-md-0">
                          <div className="upload-track bg-dark h-100">
                            <p className="d-flex justify-content-center align-items-center h-100 w-100 text-white m-0 p-0 f-med">
                              Upload new track
                            </p>
                          </div>
                        </Col>
                      </Row>
                    </div>
                    <div className="p-3">
                      <Row>
                        <Col xl={6}>
                          <div className="track-block-01 border rounded bg-theme-color">
                            <div className="p-3 border border-bottom-0">
                              <p className="text-center m-0 f-med color-black">
                                Information about track
                              </p>
                            </div>
                            <div className="form-sec bg-white">
                              <form className="pt-2 pt-5 pb-2 px-3 form-common">
                                <Form.Group>
                                  <Form.Label>Track Name</Form.Label>
                                  <Form.Control type="text" />
                                </Form.Group>
                                <Form.Group className="mb-0 mt-70">
                                  <Form.Label>Description</Form.Label>
                                  <Form.Control type="text" />
                                </Form.Group>
                                <p className="py-1 px-2 wad-tag">
                                  Write a Description
                                </p>
                                <Form.Group className="mt-60">
                                  <Form.Label>Tags</Form.Label>
                                  <Form.Control type="text" />
                                </Form.Group>
                              </form>
                            </div>
                          </div>
                        </Col>
                        <Col xl={6}>
                          <div className="track-block-01 ">
                            <div className="p-3 rounded-top bgd-primary">
                              <p className="text-white text-center m-0 f-med">
                                Choose release date
                              </p>
                            </div>
                            <div className="calendar-01 d-flex m-auto">
                              <div>
                                <p className="f-med color-black">11</p>
                                <p className="color-black">Oct</p>
                              </div>
                              <div>
                                <p className="f-med color-black">12</p>
                                <p className="color-black">Oct</p>
                              </div>
                              <div>
                                <p className="f-med color-black">13</p>
                                <p className="color-black">Oct</p>
                              </div>
                              <div className="active">
                                <p className="f-med color-black">14</p>
                                <p className="color-black">Oct</p>
                              </div>
                              <div>
                                <p className="f-med color-black">15</p>
                                <p className="color-black">Oct</p>
                              </div>
                            </div>
                            <div className="bottom-play-bar border-0 visible">
                              <ul className="range-slider-bg mb-0">
                                <li className="price-range w-100">
                                  <div className="selecteurPrix d-flex align-items-center w-100">
                                    <div className="range-slider my-4">
                                      <input
                                        type="range"
                                        className="input-range"
                                        id="customRange1"
                                      />
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                            <div className="table-box">
                              <Table>
                                <tbody>
                                  <tr>
                                    <td className="table-hd">
                                      Wav track cost{" "}
                                      <span className="ml-3 que-rd">?</span>
                                    </td>
                                    <td className="f-med clr-grn">$ 4.12</td>
                                  </tr>
                                  <tr>
                                    <td className="table-hd">
                                      Mp3 track cost{" "}
                                      <span className="ml-3 que-rd">?</span>
                                    </td>
                                    <td className="f-med clr-grn">$ 2,47</td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>
                            <h3 className="music-quality m-4">
                              {" "}
                              HIP-HOP{" "}
                              <Image
                                className="px-3 w-60px"
                                src="assets/images/genres-ion.png"
                              />{" "}
                            </h3>
                            <div className="time-box">
                              <Row className="align-item-center">
                                <Col md={6}>
                                  <p className="f-med">22:22</p>
                                </Col>
                                <Col md={6}>
                                  <p className="f-med color-black">
                                    Prelistening time
                                  </p>
                                </Col>
                              </Row>
                            </div>
                          </div>
                        </Col>
                        <Col xl={12}>
                          <Row className="mt-4 up-track-btns">
                            <Col md={5} className="mb-0 mb-md-4">
                              <button className="btn btn-primary ">
                                Choose track (WAV)
                              </button>
                            </Col>
                            <Col md={4} className="mb-0 mb-md-4">
                              <button className="btn border border-primary color-black">
                                Choose Cover
                              </button>
                            </Col>
                            <Col md={3} className="mb-0 mb-md-4">
                              <button className="btn btn-light-danger">
                                Upload
                              </button>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </Col>
                <Col xl={3} className="set-width">
                  <div className="top-tracks right-outer upload-hd-top">
                    <Card.Header className="card-header-new bg-dark text-center">
                      <h5>Top 100 Tracks</h5>
                    </Card.Header>
                    <Card.Body>
                      <div className="top-tracks-list mb-3 px-3">
                        <Row className="align-items-center flex-nowrap">
                          <Col xl={2} md={3} className="p-0">
                            <div className="count-inner active">
                              <a className="position-relative" href="#0">
                                <Image src="assets/images/PlayrA2.png" />
                                <div className="position-absolute count-num">
                                  <h6>1</h6>
                                </div>
                                <div className="overlay-new">
                                  <span className="icon">
                                    <Image src="assets/images/Vector-new.png" />
                                  </span>
                                </div>
                              </a>
                            </div>
                          </Col>
                          <Col xl={8} md={6}>
                            <div className="artist-new">
                              <h4>
                                <a href="#0">Artist</a>
                              </h4>
                              <p>Happy time</p>
                            </div>
                          </Col>
                          <Col xl={2} md={3} className="p-0">
                            <div className="track-cart-1 bg-dark text-center">
                              <a href="#0">
                                <Image
                                  className="w-20"
                                  src="assets/images/track-images/cart.png"
                                />
                                <p className="color-green font-size-11">
                                  $ 5.16
                                </p>
                              </a>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Card.Body>
                    <Card.Footer className="card-footer-new text-center">
                      <h5>
                        <a className="text-white" href="#0">
                          View top 100
                        </a>
                      </h5>
                    </Card.Footer>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
