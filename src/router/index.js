
import Vue from "vue";
import Router from "vue-router";
import store from "../store/auth.module";
import dashboardRoutes from "../dashboard/index";

const ifAuthenticated = (to, from, next) => {
  if (store.state.token) {
    next();
    return;
  }
  next("/login");
};

Vue.use(Router);

export default new Router({
  routes: [
    dashboardRoutes,
    {
      name: "home",
      path: "/",
      component: () => import("@/views/Home")
    },
    {
      name: "login",
      path: "/login",
      component: () => import("@/views/Login")
    },
    {
      name: "register",
      path: "/register",
      component: () => import("@/views/Register")
    },
    {
      name: "contact",
      path: "/contact",
      component: () => import("@/views/Contact")
    },
    {
      name: "terms",
      path: "/terms",
      component: () => import("@/views/Terms")
    },
    {
      name: "privacyPolicy",
      path: "/privacyPolicy",
      component: () => import("@/views/PrivacyPolicy")
    },
    {
      name: "about",
      path: "/about",
      component: () => import("@/views/About")
    },
    {
      name: "blog",
      path: "/blog",
      component: () => import("@/views/Blog")
    },
    {
      name: "blogDetail",
      path: "/blog/:id",
      component: () => import("@/views/BlogDetail")
    },
    {
      name: "itemDetail",
      path: "/item/:id",
      component: () => import("@/views/ItemDetail")
    },
    {
      name: "items",
      path: "/items",
      component: () => import("@/views/Items")
    },
    {
      name: "searchResult",
      path: "/searchResult/:title/:location/:category",
      component: () => import("@/components/SearchResult"),
      props: true
    },
    {
      name: "forgotPassword",
      path: "/forgotPassword",
      component: () => import("@/views/ForgotPassword")
    },
    {
      name: "forgotPasswordReset",
      path: "/forgotPasswordReset/:token",
      component: () => import("@/views/ForgotPasswordReset"),
      props: true
    },
    {
      name: "postAd",
      path: "/postAd",
      beforeEnter: ifAuthenticated,
      component: () => import("@/views/PostAd"),
      children: [
        {
          name: "userProfile",
          path: "userProfile",
          beforeEnter: ifAuthenticated,
          component: () => import("@/components/UserProfile")
        }
      ]
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
});
